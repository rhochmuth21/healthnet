/**
 * Created by Adam on 12/5/2015.
 */

$(document).ready(function() {
  setupNewMessageScripts();
});

function setupNewMessageScripts() {

  $("#new-message-button").click(function(e) {
    e.preventDefault();

    var btn = document.getElementById('new-message-button');
    var username = btn.getAttribute("username");

    $.ajax({
        url: "/messaging/" + username + "/newmessage",
        data: {
        },
        success: function (data) {
          $('#new-message-content').html(data.html);
          forceInputSetup(document.getElementById("new-message-content"));
          $('#new-message-content').append("<input id=\"messageinfo\" type=\"hidden\" username=\"" +
          username + "\">");
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
  });
  $("#message-send").click(function(e) {
    if ($('#new-message-popup-form')[0].checkValidity() == true) {
      e.preventDefault();
      var hold = document.getElementById('username-holder');
      var username = hold.getAttribute("username");

      $.ajax({
          url: "/messaging/" + username + "/newmessage",
          type: "POST",
          data: $('#new-message-popup-form').serialize(),
          success: function (data) {
            $('#jpop-faded-bg').click();
          },
          error: function (jqXHR, textStatus, errorThrown) {
              alert("Message could not be sent. Please alert your local computer-guy.")
          }
      });
    }
  });
}
