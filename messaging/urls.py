"""
HealthNet Appointments urls
"""

from django.conf.urls import url
from messaging import views

app_name = 'messaging'  # namespace

urlpatterns = [
    url(r'^(?P<username>\w+)/newmessage$', views.newMessage, name='newMessage'),
]
