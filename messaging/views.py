import json

from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils import timezone

from profiles.models import *
from .forms import *
from .models import *
from log.views import createLogEvent

def newMessage(request, username):
    if request.user.is_authenticated():
        person = request.user.healthnetuser

        if request.method == 'POST':
            message = Message()
            message.sender = person
            message.receiver = Contact.objects.get(pk=request.POST['receiver']).user
            message.body = request.POST['body']
            message.subject = request.POST['subject']
            message.send_date_time = timezone.now()


            message.save()
            createLogEvent(request.user.username, request.user.username, 21, "Message sent")

            return HttpResponse()

        else:
            form = NewMessageForm()

            mimetype = 'application/json'

            people = Contact.objects.filter(relation='se').exclude(user_id=person.id)

            html = render_to_string('messaging/ajax_new_message.html', {'form': form, 'people': people})

            res = {'html': html}
            return HttpResponse(json.dumps(res), mimetype)
