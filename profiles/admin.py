from django.contrib import admin

from .models import *


class ContactInline(admin.TabularInline):
    model = Contact
    extra = 1


class StaffInline(admin.TabularInline):
    model = Staff
    extra = 0


class PatientAdmin(admin.ModelAdmin):
    inlines = [ContactInline]


class StaffAdmin(admin.ModelAdmin):
    inlines = [ContactInline]


class HospitalAdmin(admin.ModelAdmin):
    inlines = [StaffInline]


admin.site.register(Patient, PatientAdmin)
admin.site.register(Staff, StaffAdmin)
admin.site.register(Hospital, HospitalAdmin)
