# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import profiles.helpers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('firstName', models.CharField(max_length=32)),
                ('middleInitial', models.CharField(blank=True, max_length=1, null=True)),
                ('lastName', models.CharField(max_length=64)),
                ('phoneNumber', models.CharField(max_length=14)),
                ('street', models.CharField(max_length=64)),
                ('city', models.CharField(max_length=32)),
                ('state', models.CharField(max_length=2)),
                ('zipcode', models.CharField(max_length=10)),
                ('relation', models.CharField(choices=[('ga', 'Guardian'), ('sp', 'Spouse'), ('fa', 'Father'), ('mo', 'Mother'), ('si', 'Sibling'), ('ch', 'Child'), ('ot', 'Other'), ('se', 'Self')], max_length=2)),
                ('type', models.CharField(choices=[('e', 'Emergency'), ('d', 'Doctor'), ('n', 'Nurse'), ('p', 'Patient')], max_length=1)),
            ],
        ),
        migrations.CreateModel(
            name='HealthNetUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('accountType', models.CharField(choices=[('P', 'Patient'), ('D', 'Doctor'), ('A', 'Admin'), ('N', 'Nurse')], default='P', max_length=1)),
                ('isNew', models.BooleanField(default=True)),
                ('shownTutorial', models.BooleanField(default=False)),
                ('photo', models.ImageField(default='/static/images/profile.png', upload_to=profiles.helpers.randomFileName)),
            ],
        ),
        migrations.CreateModel(
            name='Hospital',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('street', models.CharField(max_length=64)),
                ('city', models.CharField(max_length=32)),
                ('state', models.CharField(max_length=2)),
                ('zipcode', models.CharField(max_length=10)),
                ('phoneNumber', models.CharField(max_length=10)),
                ('numVisits', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('healthnetuser_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='profiles.HealthNetUser')),
                ('birthDate', models.DateField()),
                ('heightFeet', models.IntegerField()),
                ('heightInches', models.IntegerField()),
                ('weight', models.IntegerField()),
                ('insuranceCompany', models.CharField(max_length=200)),
                ('insuranceId', models.CharField(max_length=200)),
                ('numVisits', models.IntegerField(default=0)),
                ('hospital', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hospital', to='profiles.Hospital')),
                ('hospitalPref', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hospitalPref', to='profiles.Hospital')),
            ],
            bases=('profiles.healthnetuser',),
        ),
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('healthnetuser_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='profiles.HealthNetUser')),
                ('bio', models.TextField(null=True)),
                ('hospital', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='profiles.Hospital')),
            ],
            bases=('profiles.healthnetuser',),
        ),
        migrations.AddField(
            model_name='healthnetuser',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='contact',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='profiles.HealthNetUser'),
        ),
    ]
